var gzippo = require('gzippo');
var express = require('express');
var app = express();
var path = require('path');
 
  app.use(gzippo.staticGzip("" + __dirname + "/dist"));
  // app.use(express.static(path.join(__dirname, 'node_modules')));
  app.use('/scripts', express.static(__dirname + '/node_modules/'));
  app.listen(process.env.PORT || 5000);
  console.log("listening on port: " + process.env.PORT);
  