import { Component, OnInit } from '@angular/core';
import { Profile } from 'app/models/profile.model';
import { ProfileService } from 'app/shared/profile.service';

@Component({
  selector: 'app-popup-profile',
  templateUrl: './popup-profile.component.html',
  styleUrls: ['./popup-profile.component.scss']
})
export class PopupProfileComponent implements OnInit {

  profile: Profile;
  constructor(private profileService: ProfileService) { }

  ngOnInit() {
    this.profileService.getPopUpProfile().subscribe((data)=>{
      this.togglePopUp(data);
    });
  }

  togglePopUp(data) {
     if(data) {
      this.profile = data;
    }
    else{
      this.profile = null;
    }
  }

}
