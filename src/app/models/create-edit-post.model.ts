export class CreateEditPost {
    constructor(
    public title?,
    public description?,
    public picUrl?,
    public videoUrl?,
    public id?) {}
}
