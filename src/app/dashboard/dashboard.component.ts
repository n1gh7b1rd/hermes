import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from 'app/models/post.model';
import {PostsService} from 'app/shared/posts.service';
declare var faker: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  posts: Post[] = [];
  sidePosts: Post[] = [];
  calendarVisibility = false;
  @Output() toggleCalendar =  new EventEmitter();
  constructor(private postsService: PostsService) {

  }

  mainLogo: string;
  ngOnInit() {
    this.mainLogo = faker.image.business();
    this.postsService.getPosts()
      .subscribe(data => {
        this.posts = data;
        this.sidePosts = data;
      });
  }

  showCalendar() {
    this.toggleCalendar.emit(true);
  }

  hideCalendar() {
    this.toggleCalendar.emit(false);
  }
}
