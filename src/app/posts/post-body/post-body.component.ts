import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {Post} from 'app/models/post.model';
import { PostsService } from 'app/shared/posts.service';

@Component({
  selector: 'app-post-body',
  templateUrl: './post-body.component.html',
  styleUrls: ['./post-body.component.scss']
})
export class PostBodyComponent implements OnInit, OnChanges {

  @Input() post: Post;
  @Input() displayCats;
  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  popUpPost() {
    this.postsService.popUpPost(this.post);
  }

  ngOnChanges(changes) {
    if(changes.post.currentValue !== undefined) {
      this.post = changes.post.currentValue;
    }
  }
}
