import { Component, OnInit, Input } from '@angular/core';
import {Post} from 'app/models/post.model';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  @Input() posts: Post[];
  constructor() { }

  ngOnInit() {
  }

}
