import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import {AppSettings} from '../app.settings';
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";

@Injectable()
export class HttpService {

  private postHeaders: Headers = new Headers();
  private getHeaders : Headers = new Headers();
  private defaultGetHeaders = { headers: this.getHeaders, withCredentials: true };
  private defaultPostHeaders = { headers: this.postHeaders, withCredentials: true };

  constructor(private http: Http) {
    this.postHeaders.append('Content-Type', 'application/json');
  }

  public get(endpoint: string, options?:Object) {
    let opts = Object.assign(this.defaultGetHeaders, options || {});
    return this.http.get(AppSettings.API_URL + endpoint, opts)
      .map((response: Response) => response.json());
  }

  public post(endpoint: string, data: any, options?:Object) {
    let opts = Object.assign(this.defaultPostHeaders, options || {});
    return this.http.post(AppSettings.API_URL + endpoint, data, opts)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    return Observable.throw(error);
  }
}
