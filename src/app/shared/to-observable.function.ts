import { Observable } from "rxjs/Rx";

export const toObservable = function(data: any) {
  return Observable.create(function (observer) {
      observer.next(data);
      observer.complete();
    });
}
