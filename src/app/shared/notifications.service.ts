import { Injectable } from '@angular/core';
import {toObservable} from 'app/shared/to-observable.function';
import { Notification } from 'app/models/notification.model';
import { Observable, Subject } from "rxjs/Rx";

@Injectable()
export class NotificationsService {
  notifications: Notification[] = [];
  newNotification = new Subject<any>();

  public getNotification() : Observable<any> {
    return this.newNotification.asObservable();
  }

  constructor() {
    //this.notifications.push(new Notification(99, 'Umbrella Corp. likes your announcement!', picSrc, 1));
  }
  public getAll() {
    //bug but its ok, this calls the subscriber each time notifications changes, so there is no need for the getNotification subscribtion!
    return toObservable(this.notifications);
  }

  public add() {
    let newNotification = this.spawnNotification();
    this.notifications.push(newNotification);
    this.newNotification.next(newNotification);
  }

  public spawnNotification() {
    let picSrc = 'https://vignette3.wikia.nocookie.net/villains/images/8/8a/The_Umbrella_Logo.jpg/revision/latest?cb=20140510225737';
    return new Notification(this.notifications.length+1, 'Umbrella Corp. likes your publication!', picSrc, 1);
  }

}
