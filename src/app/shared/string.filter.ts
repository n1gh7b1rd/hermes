import {Injectable, Pipe, PipeTransform} from '@angular/core';
import * as _ from 'lodash';

@Pipe({ name: 'string' })

@Injectable()
export class StringFilter implements PipeTransform {
  transform(items: any[], predicate: string): any {
    if(!predicate) return items;

    predicate = predicate.toLowerCase();

    return _.filter(items, item => {
      if(_.includes(item.description.toLowerCase(), predicate))
        return true;

      if(item.categories.indexOf(predicate) !== -1)
        return true;

      return false;
    });
  }
}
