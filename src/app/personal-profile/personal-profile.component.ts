import { Component, OnInit } from '@angular/core';
declare var faker: any;

@Component({
  selector: 'app-personal-profile',
  templateUrl: './personal-profile.component.html',
  styleUrls: ['./personal-profile.component.scss']
})
export class PersonalProfileComponent implements OnInit {

  description: string = faker.lorem.sentences(20);
  profilePicture: string = faker.image.abstract();
  profileName: string = faker.company.companyName();
  availableCategories = [
    'ISP', 'Software Development', 'networking'
  ].map(cat => { return { selected: false, name: cat }; });

  constructor() { }

  ngOnInit() {
  }

}
