export const tabsConfig = [
  {
    state: 'news',
    label: 'News'
  },
  {
    state: 'see',
    label: 'See'
  },
  {
    state: 'say',
    label: 'Say'
  },
  {
    state: 'hear',
    label: 'Hear'
  },
  {
    state: 'profile',
    label: 'Profile'
  }
];
