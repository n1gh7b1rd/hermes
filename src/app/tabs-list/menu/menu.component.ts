import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { menuConfig } from 'app/app.menu.config';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Output() itemClicked = new EventEmitter();
  items = menuConfig;
  constructor() { }

  ngOnInit() {
  }

  itemClick(action){
    // temp only for calendar
    if(action.toString().toLowerCase()== "calendar"){
      this.itemClicked.emit(action);
      console.log("clicked calendar");
    }
  }
}
