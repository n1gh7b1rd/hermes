import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Notification } from 'app/models/notification.model';
import { ProfileService } from 'app/shared/profile.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  @Input() notification: Notification;
  constructor(private profileService: ProfileService) { }

  @Output() onClose = new EventEmitter<any>();
  ngOnInit() {
  }

  popUpProfile() {
    this.profileService.popUpProfile(this.notification.id);
  }

  close() {
    this.onClose.emit(this.notification);
  }

}
